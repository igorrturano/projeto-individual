const express = require("express")
const router = express.Router()
const userList = require('../../models/user')

router.get('/', (req,res) => {
    try{
        res.send(userList)
    } catch (error) {
        res.status(500).send({"error": "Server Error"})
    }
    
})

router.get('/:userId', (req,res) => {
    try{
        let usuario = userList.filter(u => u.id == req.params["userId"])
        if (usuario.length > 0) {
            res.send(usuario[0])
        } else {
            res.status(404).send({"Error": "user not found"})
        }
    }catch(err) {
        res.status(500).send({"Error": "Server Error"})
    }

})    

router.post('/', (req,res) => {
    try{
        const usuario = {id: 0} 
        const {email, nome, idade} = req.body
        usuario.email = email
        usuario.nome = nome
        usuario.idade = idade
        console.log(`email: ${email}, nome: ${nome}, idade: ${idade}`)
        if (!email) {
            res.status(400).send("invalid e-mail")
        }else if (!nome) {
            res.status(400).send("invalid name")
        }else if (!idade || idade == NaN) {
            res.status(400).send("invalid age")
        }
        else{
            usuario.id = userList[userList.length-1].id +1
            userList.push(usuario)
            res.send(userList)
        }
    }catch(err) {
        res.status(500).send({"Error": "Server Error"})
    }
})

module.exports = router