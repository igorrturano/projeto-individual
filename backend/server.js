const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const PORT = 42000

app.use(express.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use('/', require('./routes/hello'))
app.use('/user', require('./routes/api/user'))


app.listen(42000, console.log(`Conectado na porta ${PORT}`))